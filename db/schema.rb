# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20101223032748) do

  create_table "ad_spots", :force => true do |t|
    t.string   "export_file_name"
    t.string   "spot_file_name"
    t.string   "spot_content_type"
    t.integer  "spot_file_size"
    t.integer  "spot_length_ms"
    t.integer  "segment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "psa",               :default => false
  end

  create_table "greeters", :force => true do |t|
    t.boolean  "available",  :default => true
    t.string   "em_handle"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "segment_hosts", :force => true do |t|
    t.boolean  "available"
    t.integer  "segment_id"
    t.string   "em_handle"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_count", :default => 0
  end

  create_table "segments", :force => true do |t|
    t.string   "state"
    t.string   "highway"
    t.integer  "number"
    t.float    "lat_start"
    t.float    "lng_start"
    t.float    "lat_stop"
    t.float    "lng_stop"
    t.string   "description"
    t.float    "dst"
    t.integer  "num_adj"
    t.string   "city"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "layer",       :default => 0
  end

  create_table "users", :force => true do |t|
    t.string   "skype_name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "logged_in",          :default => false
    t.integer  "current_segment_id"
    t.integer  "current_greeter_id"
    t.string   "phone"
  end

end
