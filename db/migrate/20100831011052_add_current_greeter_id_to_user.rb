class AddCurrentGreeterIdToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :current_greeter_id, :integer
  end

  def self.down
    remove_column :users, :current_greeter_id
  end
end
