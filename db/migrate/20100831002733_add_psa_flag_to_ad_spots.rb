class AddPsaFlagToAdSpots < ActiveRecord::Migration
  def self.up
    add_column :ad_spots, :psa, :boolean, :default => false

    AdSpot.update_all("psa = 'f'")
  end

  def self.down
    remove_column :ad_spots, :psa
  end
end
