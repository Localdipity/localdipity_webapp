class AddLayerToSegment < ActiveRecord::Migration
  def self.up
    add_column :segments, :layer, :integer, :default => 0
    Segment.all.each { |e| e.update_attribute(:layer, 0) }

    change_column :segments, :lat_start, :float
  end

  def self.down
    remove_column :segments, :layer
  end
end
