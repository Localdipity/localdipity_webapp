class CreateAdSpots < ActiveRecord::Migration
  def self.up
    create_table :ad_spots do |t|
      t.string :export_file_name
      t.string :spot_file_name
      t.string :spot_content_type
      t.integer :spot_file_size
      t.integer :spot_length_ms
      t.integer :segment_id

      t.timestamps
    end
  end

  def self.down
    drop_table :ad_spots
  end
end
