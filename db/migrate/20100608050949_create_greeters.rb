class CreateGreeters < ActiveRecord::Migration
  def self.up
    create_table :greeters do |t|
      t.boolean :available, :default => true
      t.string :em_handle

      t.timestamps
    end
  end

  def self.down
    drop_table :greeters
  end
end
