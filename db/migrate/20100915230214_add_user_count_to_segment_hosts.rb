class AddUserCountToSegmentHosts < ActiveRecord::Migration
  def self.up
    add_column :segment_hosts, :user_count, :integer, :default => 0
  end

  def self.down
    remove_column :segment_hosts, :user_count
  end
end
