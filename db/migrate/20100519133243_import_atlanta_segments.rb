class ImportAtlantaSegments < ActiveRecord::Migration
  def self.up
    FasterCSV.foreach("metro_atlanta_start.csv", :headers => true) do |row|
      Segment.create(row.to_hash)
    end
  end

  def self.down
    Segment.destroy_all
  end
end
