class AddLoggedInAndCurrentSegmentToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :logged_in, :boolean, :default => false
    add_column :users, :current_segment_id, :integer
  end

  def self.down
    remove_column :users, :current_segment_id
    remove_column :users, :logged_in
  end
end
