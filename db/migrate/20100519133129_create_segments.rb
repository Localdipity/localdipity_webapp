class CreateSegments < ActiveRecord::Migration
  def self.up
    create_table :segments do |t|
      t.string :state
      t.string :highway
      t.integer :number
      t.decimal :lat_start
      t.float :lng_start
      t.float :lat_stop
      t.float :lng_stop
      t.string :description
      t.float :dst
      t.integer :num_adj
      t.string :city

      t.timestamps
    end
  end

  def self.down
    drop_table :segments
  end
end
