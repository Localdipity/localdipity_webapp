class CreateSegmentHosts < ActiveRecord::Migration
  def self.up
    create_table :segment_hosts do |t|
      t.boolean :available
      t.integer :segment_id
      t.string :em_handle

      t.timestamps
    end
  end

  def self.down
    drop_table :segment_hosts
  end
end
