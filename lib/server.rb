require 'rubygems'
require 'active_record'
require 'eventmachine'
require 'geokit'

$LOAD_PATH << 'vendor/plugins/geokit-rails/lib/geokit-rails'
$LOAD_PATH << 'vendor/plugins/geokit-rails/lib'
require 'geokit-rails.rb'

require 'app/models/localdipity_parser.rb'
require 'app/models/greeter.rb'
require 'app/models/segment.rb'
require 'app/models/segment_host.rb'
require 'app/models/user.rb'
# can't easily include AdSpot without worrying about the paperclip files so we
# just redeclare it here
class AdSpot < ActiveRecord::Base
  belongs_to :segment
end

PORT = 40044

$greeters = {}
$segment_hosts = {}
$lp = LocaldipityParser.new

module SwitchServer
  def post_init
    puts "-- #{DateTime.now} someone, #{self.object_id}, connected to the echo server!"
  end

  def receive_data data
    puts "\n\n\n"
    puts "========= RECEIVED(#{self.object_id}): '#{data.chomp}'\n"
    send_data ">>>ECHO: you sent: '#{data.chomp}'\n"

    # backdoor, used for testing java segment host code
    # if data.match(/COMMAND:.*/)
    #   name = data.split(":")[1]
    #   puts "splitting name: #{name}"
    #   puts "sending 'CALL: #{name}' to #{$segment_hosts.keys.first}"
    #   puts "size: #{$segment_hosts.keys.size}"
    #   $segment_hosts.values.first.send_data "CALL: #{name}"
    #   return
    # end

    # e.g., { :self => "LOGIN SUCCESS",
    #         :em_handle_as_string => "CALL <skype_name>",
    #         :new_greeter => "LOGIN SUCCESS",
    #         :new_segment_host => "LOGIN SUCCESS" }
    result = $lp.parse(self.object_id.to_s, data)
    result.each_pair do |connection,response|
      puts "========= RESPONSE(#{connection}): '#{response.chomp}'\n"
      case connection

      when :self # we only track greeters so can't look up by object_id, need :self
        send_data response

      when :new_greeter
        send_data response
        $greeters.merge!({self.object_id.to_s => self})

      when :new_segment_host
        send_data response
        $segment_hosts.merge!({self.object_id.to_s => self})

      else # send to another connection
        # look up connection and send response
        conn = $greeters[connection]
        conn = $segment_hosts[connection] if conn.nil?

        conn.send_data response
      end
    end
  end

  def unbind
    g = Greeter.find_by_em_handle(self.object_id.to_s)
    if g
      p "removing GREETER, #{self.object_id}, from greeters list"
      g.destroy
      $greeters.delete(self.object_id)
    end

    h = SegmentHost.find_by_em_handle(self.object_id.to_s)
    if h
      p "removing HOST, #{self.object_id}, from host list"
      h.destroy
      $segment_hosts.delete(self.object_id)
    end
  end
end

def connect(environment = 'production')
  conf = YAML::load(File.open(File.dirname(__FILE__) + '/../config/database.yml'))
  ActiveRecord::Base.establish_connection(conf[environment])
end

EventMachine::run {
  connect

  # if the switch has just come online, we can have no greeters or segment hosts..
  Greeter.destroy_all
  SegmentHost.destroy_all
  User.update_all("logged_in = 'false'")
  User.update_all("current_segment_id = NULL")

  EventMachine::start_server "0.0.0.0", PORT, SwitchServer
  
  puts "User count: #{User.count}"
  puts "Segment count: #{Segment.count}"

  EventMachine::add_periodic_timer(30) { 
    $greeters.each_pair do |gid,greeter|
      begin
        # ping greeter's to verify they are alive
        p "sending hello #{DateTime.now} to #{gid}"
        greeter.send_data "HELLO\n"
      rescue
        p "*"*80
        p gid
        p $!

        # if they are not, kill the AR record
        g = Greeter.find_by_em_handle(gid)
        g.destroy
        $greeters.delete(gid)
      end
    end
  }
}
