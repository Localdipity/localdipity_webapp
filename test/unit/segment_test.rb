require 'test_helper'

class SegmentTest < ActiveSupport::TestCase
  test "should return higher value layer's first" do
    big_segment = Segment.create!(:lat_start => 10,
                                  :lat_stop  => 30,
                                  :lng_start => 10,
                                  :lng_stop  => 30)
    small_segment = Segment.create!(:lat_start => 15,
                                    :lat_stop  => 25,
                                    :lng_start => 15,
                                    :lng_stop  => 25,
                                    :layer => 1)

    assert_equal small_segment, Segment.bounder(20,20)
  end
end
