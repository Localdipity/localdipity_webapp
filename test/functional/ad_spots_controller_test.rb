require 'test_helper'

class AdSpotsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ad_spots)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ad_spot" do
    assert_difference('AdSpot.count') do
      post :create, :ad_spot => { }
    end

    assert_redirected_to ad_spot_path(assigns(:ad_spot))
  end

  test "should show ad_spot" do
    get :show, :id => ad_spots(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => ad_spots(:one).to_param
    assert_response :success
  end

  test "should update ad_spot" do
    put :update, :id => ad_spots(:one).to_param, :ad_spot => { }
    assert_redirected_to ad_spot_path(assigns(:ad_spot))
  end

  test "should destroy ad_spot" do
    assert_difference('AdSpot.count', -1) do
      delete :destroy, :id => ad_spots(:one).to_param
    end

    assert_redirected_to ad_spots_path
  end
end
