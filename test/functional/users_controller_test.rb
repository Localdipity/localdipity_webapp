require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post :create, :user => { :email => 'sam@example.com',
                               :phone => '7063630863'
      }
    end

    assert_redirected_to user_path(assigns(:user))
  end
  test "should create user and update their location" do
    assert_difference('User.count') do
      post :create, :user => { :email => 'sam@example.com',
                               :phone => '7063630863' },
                    :lat_lng => "83,33"
    end

    assert_redirected_to user_path(assigns(:user))
  end



  test "should show user" do
    get :show, :id => users(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => users(:one).to_param
    assert_response :success
  end

  # test "should update user looking up by id" do
  #   # Create one segment so user can be placed into a default segment
  #   Segment.create(:description => "Metro Atlanta",
  #                  :lat_start   => 0,
  #                  :lng_start   => 0,
  #                  :lat_stop    => 100,
  #                  :lng_stop    => 100)

  #   user = users(:one)
  #   post :update_location, :id      => user.to_param,
  #                          :lat_lng => "83,33"

  #   assert_response :success
  #   assert_not_nil user.reload.current_segment
  # end

  test "should update user looking up by phone number" do
    # Create one segment so user can be placed into a default segment
    Segment.create(:description => "Metro Atlanta",
                   :lat_start   => 0,
                   :lng_start   => 0,
                   :lat_stop    => 100,
                   :lng_stop    => 100)

    user = users(:one)
    post :update_location, :phone   => user.phone,
                           :lat_lng => "83,33"

    assert_response :success
    assert_not_nil user.reload.current_segment
  end

  # test "should update user" do
  #   put :update, :id => users(:one).to_param, :user => { }
  #   assert_redirected_to user_path(assigns(:user))
  # end

  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete :destroy, :id => users(:one).to_param
    end

    assert_redirected_to users_path
  end
end
