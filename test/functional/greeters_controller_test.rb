require 'test_helper'

class GreetersControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:greeters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create greeter" do
    assert_difference('Greeter.count') do
      post :create, :greeter => { }
    end

    assert_redirected_to greeter_path(assigns(:greeter))
  end

  test "should show greeter" do
    get :show, :id => greeters(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => greeters(:one).to_param
    assert_response :success
  end

  test "should update greeter" do
    put :update, :id => greeters(:one).to_param, :greeter => { }
    assert_redirected_to greeter_path(assigns(:greeter))
  end

  test "should destroy greeter" do
    assert_difference('Greeter.count', -1) do
      delete :destroy, :id => greeters(:one).to_param
    end

    assert_redirected_to greeters_path
  end
end
