require 'test_helper'

class SegmentHostsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:segment_hosts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create segment_host" do
    assert_difference('SegmentHost.count') do
      post :create, :segment_host => { }
    end

    assert_redirected_to segment_host_path(assigns(:segment_host))
  end

  test "should show segment_host" do
    get :show, :id => segment_hosts(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => segment_hosts(:one).to_param
    assert_response :success
  end

  test "should update segment_host" do
    put :update, :id => segment_hosts(:one).to_param, :segment_host => { }
    assert_redirected_to segment_host_path(assigns(:segment_host))
  end

  test "should destroy segment_host" do
    assert_difference('SegmentHost.count', -1) do
      delete :destroy, :id => segment_hosts(:one).to_param
    end

    assert_redirected_to segment_hosts_path
  end
end
