set :application, "treesatlanta"
set :deploy_to, "/var/www/apps/#{application}"
set :rails_env, "treesatlanta_production"

desc "Restart the web server. Overrides the default task for Site5 use"
deploy.task :restart, :roles => :app do
  run "touch #{current_path}/tmp/restart.txt"
end
