class AppSettings
  TROPO_VOICE_TOKEN = '135bffe9c3f3064384acc71798ab80a943fa30ebf94bc756ecfecb8256b084f11657cc608d3bf5a86b6501af'
  TROPO_API = 'http://api.tropo.com/1.0/sessions'

  CALL_TYPE = { 
    :conference         => 'conference',
    :greeter            => 'greeter',
    :greeter_conference => 'greeter_conference' 
  }
end
