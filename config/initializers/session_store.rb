# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_localdipity_session',
  :secret      => 'e5553f2345c76d5cfea0fc59c002c05edc711479e6a45bff01e74e5f8a29369f6b2a539de3d628c1788f2b4c25d08f67e369f546cf6988350871beb613032cf7'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
