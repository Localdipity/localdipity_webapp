require 'capistrano/ext/multistage'

# Necessary to run on Site 5
set :use_sudo, false
set :group_writable, false

# Less releases, less space wasted
set :keep_releases, 2

# thanks to http://www.rubyrobot.org/article/deploying-rails-20-to-mongrel-with-capistrano-21
set :runner, nil

set :application, "localdipity"
set :user, "deploy"
set :repository,  "git@github.com:wallace/localdipity-webapp.git"
set :deploy_to, "/var/www/apps/#{application}"
set :deploy_via, :remote_cache
set :scm, :git
set :scm_passphrase, "mAtha7he"
default_run_options[:pty] = true

role :app, "app.localdipity.com"
role :web, "app.localdipity.com"
role :db,  "app.localdipity.com", :primary => true

desc "Restart the web server. Overrides the default task for Site5 use"
deploy.task :restart, :roles => :app do
  run "touch #{current_path}/tmp/restart.txt"
end
