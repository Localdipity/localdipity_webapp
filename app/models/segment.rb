class Segment < ActiveRecord::Base
  has_one :segment_host
  has_many :ad_spots, :conditions => { :psa => false }
  has_many :psas, :class_name => "AdSpot", :conditions => { :psa => true }
  has_many :current_users, :class_name => "User", :foreign_key => "current_segment_id"

  before_validation :insure_sw_ne

  named_scope :exclude, lambda {|description| { :conditions => ["#{self.table_name}.description != ? or #{self.table_name}.description IS NULL", description]}}
  named_scope :order_by_layer, lambda {{ :order => "layer DESC" }}

  def insure_sw_ne
    # if the start latitude is greater than the stop latitude
    if self.lat_start.to_f > self.lat_stop
      # swap them
      self.lat_start, self.lat_stop = self.lat_stop, self.lat_start
    end

    # if the start longitude is greater than the stop longitude
    if self.lng_start > self.lng_stop
      # swap them
      self.lng_start, self.lng_stop = self.lng_stop, self.lng_start
    end
  end

  def sw
    GeoKit::LatLng.new(self.lat_start, self.lng_start)
  end

  def ne
    GeoKit::LatLng.new(self.lat_stop, self.lng_stop)
  end

  # find the segment that bounds this lat, lng
  def self.bounder(lat,lng)
    point = GeoKit::LatLng.new(lat,lng)

    # check the rest of the segments ignoring Metro ATL
    Segment.order_by_layer.each do |segment|
      return segment if GeoKit::Bounds.new(segment.sw, segment.ne).contains?(point)
    end

    # no other segment matches so we use Metro ATL
    atl = Segment.first(:conditions => { :description => "Metro Atlanta" })
    atl
  end

  # Convenience method as the relationship between ad_spot and segment started
  # out as 1:1 and we don't currently care which ad_spot is served up to a new
  # user as long as it is related to the appropriate segment.
  #
  # In fact, this may be a good place to implement the algorithm for choosing
  # the appropriately associated ad spot.
  #
  # At some point, it may make sense for AdSpot to be a polymorphic
  # relationship but not yet.
  def ad_spot
    self.ad_spots.first
  end

  # eventually, we'll have a pool of segment hosts
  def segment_host
    SegmentHost.first
  end

  def psa
    self.psas.first
  end

  # Returns lat_lng as comma separated string
  def lat_lng_sw
    [self.lat_start, self.lng_start].join(',')
  end

  def lat_lng_sw=(la_ll)
    self.lat_start, self.lng_start = la_ll.split(",")
  end

  # Returns lat_lng as comma separated string
  def lat_lng_ne
    [self.lat_stop, self.lng_stop].join(',')
  end

  def lat_lng_ne=(la_ll)
    self.lat_stop, self.lng_stop = la_ll.split(",")
  end

end
