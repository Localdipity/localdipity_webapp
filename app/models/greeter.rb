class Greeter < ActiveRecord::Base
  validates_uniqueness_of :em_handle
  has_one :current_user, :class_name => "User", :foreign_key => "current_greeter_id"

  named_scope :available, :conditions => { :available => true }

  # not very performant but we don't have very many greeters anyway..
  def self.random
    Greeter.all(:conditions => { :available => true }).rand
  end
end
