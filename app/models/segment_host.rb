class SegmentHost < ActiveRecord::Base
  MAX_USERS = 5

  validates_uniqueness_of :em_handle
  has_many :current_users, :class_name => "User", :foreign_key => "current_host_id"
  belongs_to :segment

  # available will only change if we place an artificial limit on it
  named_scope :available, :conditions => { :available => true }

  def full?
    user_count >= MAX_USERS
  end
end
