class AdSpot < ActiveRecord::Base
  belongs_to :segment
  has_attached_file :spot

  # validates_presence_of :export_file_name, :spot_length_ms

  # TODO: need some way to determine wav file length
end
