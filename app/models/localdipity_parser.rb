class LocaldipityParser

  # receive a string, parse it and return a hash to send back whose keys indicate
  # which connection and the values are the message to send back to that connection
  #
  # e.g., { :self => "LOGIN SUCCESS",
  #         :em_handle_as_string => "CALL <skype_name>",
  #         :new_greeter => "LOGIN SUCCESS" }
  def parse(em_handle, data)
    return {} if data.nil?

    hash = {}
    command, args = data.split(":")
    command.strip!
    args.strip! if args

    case command
    when "HOST"
      action, *arg = args.split(' ')
      case action
      when "LOGIN"
        un, pw = arg[0], arg[1]

        if un.match(/\Agreeter\.\d{3}\Z/) and "pass" == pw
          # find or create a greeter
          g = SegmentHost.find_or_create_by_em_handle(:em_handle => em_handle)
          g.update_attribute(:available, true)

          # host stuff here
          hash[:new_segment_host] = "HOST LOGIN SUCCESS\n"
        end
      else
        hash[em_handle] = ">>>INVALID COMMAND for HOST\n"
      end
    when "GREETER"
      action, *arg = args.split(' ')
      case action
      when "LOGIN"
        un, pw = arg[0], arg[1]
        if un.match(/\Agreeter\.\d{3}\Z/) and "pass" == pw
          # find or create a greeter
          g = Greeter.find_or_create_by_em_handle(:em_handle => em_handle)

          hash[:new_greeter] = "LOGIN SUCCESS\n"
        end
      when "AVAILABLE"
        if "pass" == arg[0]
          # mark connection as available
          g = Greeter.find_by_em_handle(em_handle)

          if g
            g.update_attribute(:available, true)
          else
            # greeter currently not logged in so send error
            hash[:self] = "YOU ARE NOT CURRENTLY LOGGED IN AS A GREETER.  Please check your username and / or password and try again."
          end
        end
      when "HANDOFF"
        if "pass" == arg[0]
          # mark connection as available
          g = Greeter.find_by_em_handle(em_handle)
          # get user and segment host to call user
          u = g.current_user
          segs = u.current_segment

          # now that we have the user we can clear the user association with the greeter
          g.available = true
          g.current_user = nil
          g.save!

          if segs.segment_host.blank?
            puts "ERROR: segs: #{segs.id} has no segment hosts associated with it for greeter: #{g.id}\n"
            hash[g.em_handle] = "ERROR: segs: #{segs.id} has no segment hosts associated with it for greeter: #{g.id}\n"
          elsif segs.segment_host.full?
            puts "ERROR: the segment host (#{segs.segment_host.id}) for segs (#{segs.id}) has no available space left\n"
            hash[g.em_handle] = "ERROR: the segment host (#{segs.segment_host.id}) for segs (#{segs.id}) has no available space left\n"
          else
            # mark that another user is added to the segment_host call
            segs.segment_host.user_count += 1
            segs.segment_host.save!

            # have segment's segment_host connection add user to conference call
            hash[segs.segment_host.em_handle] = "CALL: #{u.skype_name}\n"
          end
        end
      else
        hash[em_handle] = ">>>INVALID COMMAND for GREETER\n" 
      end
    when "LOGIN"
      un, pw = args.split(' ')
      valid = User.login(un,pw)

      response = if valid
                   "#{args} you are logged in\n"
                 else
                   "#{args}: no such user found\n" 
                 end
      hash[:self] = response

    when "LOGOUT"
      un, pw = args.split(' ')
      valid = User.logout(un,pw)

      response = if valid
                   "#{args} you are logged out\n"
                 else 
                   "#{args}: no such user found\n" 
                 end
      hash[:self] = response

    when "PSA_LOCATION"
      un, lat, lng = args.split(' ')

      if u = User.logged_in?(un)
        lat = lat.to_f
        lng = lng.to_f

        segs = Segment.bounder(lat,lng)
        u.update_attribute(:current_segment, segs)

        # contact available greeter and have greeter call this client and play a wav file
        hash[:self] = "you are in '#{segs.description}'\n"

        g = Greeter.random
        if g.nil?
          # whoa no greeters, what do we do?
          # send an informational message
          hash[:self] = "you are in '#{segs.description}, but all greeters are currently busy.  please try again soon.'\n"

        else
          # send msg to greeter, tell greeter to call u, mark greeter as busy

          if segs.present? and segs.psa.present?
            g.update_attribute(:available, false)
            # send wav filename and length to be played
            hash[g.em_handle] = "PSA_CALL: #{u.skype_name}:#{segs.psa.export_file_name}:#{segs.psa.spot_length_ms}\n"
          else
            hash[g.em_handle] = "ERROR: segs: #{segs.id} has no psa associated with it\n"
          end
        end
      else
        hash[:self] = "'#{un}' is not logged in.  please 'LOGIN: #{un}' first\n"
      end

    when "LOCATION"
      un, lat, lng = args.split(' ')

      if u = User.logged_in?(un)
        lat = lat.to_f
        lng = lng.to_f

        segs = Segment.bounder(lat,lng)

        # contact available greeter and have greeter call this client and play a wav file
        hash[:self] = "you are in '#{segs.description}'\n"

        g = Greeter.random
        u.update_attributes(:current_segment => segs,
                            :current_greeter => g)
        if g.nil?
          # whoa no greeters, what do we do?
          # send an informational message
          hash[:self] = "you are in '#{segs.description}, but all greeters are currently busy.  please try again soon.'\n"

        else
          # send msg to greeter, tell greeter to call u, mark greeter as busy

          if segs.present? and segs.ad_spot.present?
            g.update_attribute(:available, false)
            # send wav filename and length to be played
            hash[g.em_handle] = "CALL: #{u.skype_name}:#{segs.ad_spot.export_file_name}:#{segs.ad_spot.spot_length_ms}\n"
          else
            hash[g.em_handle] = "ERROR: segs: #{segs.id} has no ad_spot associated with it\n"
          end
        end

      else
        hash[:self] = "'#{un}' is not logged in.  please 'LOGIN: #{un}' first\n"
      end
    else
      hash[:self] = ">>>INVALID COMMAND\n"
    end

    hash
  end
end
