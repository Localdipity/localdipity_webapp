class User < ActiveRecord::Base
  belongs_to :current_segment, :class_name => "Segment"
  belongs_to :current_greeter, :class_name => "Greeter"
  belongs_to :current_host, :class_name => "SegmentHost"

  validates_presence_of :email
  validates_presence_of :phone

  # don't have a password attribute at this time
  def self.login(un, pw)
    u = User.find_by_skype_name(un)
    return false unless u

    # if a user logs in, clear out their segment as we should soon be receiving
    # their lat, lng
    u.update_attributes(:logged_in          => true,
                        :current_segment_id => nil)
  end

  def self.logout(un,pw)
    u = User.find_by_skype_name(un)
    return false unless u

    u.update_attributes(:logged_in          => false,
                        :current_segment_id => nil)
  end

  # check if user has logged in already
  def self.logged_in?(un)
    u = User.find_by_skype_name(un)
    return false unless u

    u
  end
end
