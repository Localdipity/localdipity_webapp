module SegmentsHelper
  def show_map_link(segment)
    points = [
      [segment.lat_stop  , segment.lng_stop]  , 
      [segment.lat_start , segment.lng_stop]  , 
      [segment.lat_start , segment.lng_start] , 
      [segment.lat_stop  , segment.lng_start] , 
      [segment.lat_stop  , segment.lng_stop]
    ]
    "http://maps.google.com/maps/api/staticmap?size=640x640&sensor=false&maptype=roadmap&path=color:blue|weight:5|#{points.map {|e| e.join(',')}.join('|')}"
  end
end
