module GreetersHelper
  def current_user(greeter)
    if greeter.current_user.present?
      link_to greeter.current_user.skype_name, user_path(greeter.current_user)
    else
      'no current user'
    end
  end
end
