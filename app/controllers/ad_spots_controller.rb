class AdSpotsController < ApplicationController
  # GET /ad_spots
  # GET /ad_spots.xml
  def index
    @ad_spots = AdSpot.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @ad_spots }
    end
  end

  # GET /ad_spots/1
  # GET /ad_spots/1.xml
  def show
    @ad_spot = AdSpot.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @ad_spot }
    end
  end

  # GET /ad_spots/new
  # GET /ad_spots/new.xml
  def new
    @ad_spot = AdSpot.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @ad_spot }
    end
  end

  # GET /ad_spots/1/edit
  def edit
    @ad_spot = AdSpot.find(params[:id])
  end

  # POST /ad_spots
  # POST /ad_spots.xml
  def create
    @ad_spot = AdSpot.new(params[:ad_spot])

    respond_to do |format|
      if @ad_spot.save
        flash[:notice] = 'AdSpot was successfully created.'
        format.html { redirect_to(@ad_spot) }
        format.xml  { render :xml => @ad_spot, :status => :created, :location => @ad_spot }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @ad_spot.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /ad_spots/1
  # PUT /ad_spots/1.xml
  def update
    @ad_spot = AdSpot.find(params[:id])

    respond_to do |format|
      if @ad_spot.update_attributes(params[:ad_spot])
        flash[:notice] = 'AdSpot was successfully updated.'
        format.html { redirect_to(@ad_spot) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @ad_spot.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /ad_spots/1
  # DELETE /ad_spots/1.xml
  def destroy
    @ad_spot = AdSpot.find(params[:id])
    @ad_spot.destroy

    respond_to do |format|
      format.html { redirect_to(ad_spots_url) }
      format.xml  { head :ok }
    end
  end
end
