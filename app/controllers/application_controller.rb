# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  protect_from_forgery # See ActionController::RequestForgeryProtection for details

  # Scrub sensitive parameters from your log
  # filter_parameter_logging :password

  def hit_tropo_end_point(params = {})
    params.merge!('token' => AppSettings::TROPO_VOICE_TOKEN)
    query_string = params.map {|k,v| [k,v].join('=')}.join('&')
    param_list = "?#{query_string}" if query_string.present?

    logger.error "*"*80
    logger.error "curling #{AppSettings::TROPO_API}#{param_list}"
    logger.error "*"*80

    `curl "#{AppSettings::TROPO_API}#{param_list}"`
  end
end
