class SegmentHostsController < ApplicationController
  # GET /segment_hosts
  # GET /segment_hosts.xml
  def index
    @segment_hosts = SegmentHost.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @segment_hosts }
    end
  end

  # GET /segment_hosts/1
  # GET /segment_hosts/1.xml
  def show
    @segment_host = SegmentHost.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @segment_host }
    end
  end

  # GET /segment_hosts/new
  # GET /segment_hosts/new.xml
  def new
    @segment_host = SegmentHost.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @segment_host }
    end
  end

  # GET /segment_hosts/1/edit
  def edit
    @segment_host = SegmentHost.find(params[:id])
  end

  # POST /segment_hosts
  # POST /segment_hosts.xml
  def create
    @segment_host = SegmentHost.new(params[:segment_host])

    respond_to do |format|
      if @segment_host.save
        flash[:notice] = 'SegmentHost was successfully created.'
        format.html { redirect_to(@segment_host) }
        format.xml  { render :xml => @segment_host, :status => :created, :location => @segment_host }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @segment_host.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /segment_hosts/1
  # PUT /segment_hosts/1.xml
  def update
    @segment_host = SegmentHost.find(params[:id])

    respond_to do |format|
      if @segment_host.update_attributes(params[:segment_host])
        flash[:notice] = 'SegmentHost was successfully updated.'
        format.html { redirect_to(@segment_host) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @segment_host.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /segment_hosts/1
  # DELETE /segment_hosts/1.xml
  def destroy
    @segment_host = SegmentHost.find(params[:id])
    @segment_host.destroy

    respond_to do |format|
      format.html { redirect_to(segment_hosts_url) }
      format.xml  { head :ok }
    end
  end
end
