class GreetersController < ApplicationController
  # GET /greeters
  # GET /greeters.xml
  def index
    @greeters = Greeter.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @greeters }
    end
  end

  # GET /greeters/1
  # GET /greeters/1.xml
  def show
    @greeter = Greeter.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @greeter }
    end
  end

  # GET /greeters/new
  # GET /greeters/new.xml
  def new
    @greeter = Greeter.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @greeter }
    end
  end

  # GET /greeters/1/edit
  def edit
    @greeter = Greeter.find(params[:id])
  end

  # POST /greeters
  # POST /greeters.xml
  def create
    @greeter = Greeter.new(params[:greeter])

    respond_to do |format|
      if @greeter.save
        flash[:notice] = 'Greeter was successfully created.'
        format.html { redirect_to(@greeter) }
        format.xml  { render :xml => @greeter, :status => :created, :location => @greeter }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @greeter.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /greeters/1
  # PUT /greeters/1.xml
  def update
    if params[:greeter][:current_user].present?
      params[:greeter][:current_user] = User.find(params[:greeter][:current_user])
    end
    @greeter = Greeter.find(params[:id])

    respond_to do |format|
      if @greeter.update_attributes(params[:greeter])
        flash[:notice] = 'Greeter was successfully updated.'
        format.html { redirect_to(@greeter) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @greeter.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /greeters/1
  # DELETE /greeters/1.xml
  def destroy
    @greeter = Greeter.find(params[:id])
    @greeter.destroy

    respond_to do |format|
      format.html { redirect_to(greeters_url) }
      format.xml  { head :ok }
    end
  end
end
