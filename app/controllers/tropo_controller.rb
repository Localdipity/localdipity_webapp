class TropoController < ApplicationController
  skip_before_filter :verify_authenticity_token

  # POST /tropo.json?id=1&call_type=greeter
  def create
    @user = User.find(params[:session][:parameters][:id])
    @tropo = Tropo::Generator.new
    @tropo.call :to => "+1#{@user.phone}", :from => "+14045458740"

    # 0. Have user press a button
    @tropo.ask :name => 'zip', :bargein => true, :timeout => 60, :attempts => 2,
        :say => [{:event => "timeout", :value => "Sorry, I did not hear anything."},
                 {:event => "nomatch:1 nomatch:2", :value => "Oops, that wasn't a one-digit number."},
                 {:value => "Please press any number to continue."}],
                  :choices => { :value => "[1 DIGITS]"}

    case params[:session][:parameters][:call_type]
    when 'greeter'
      # 1. Play greeter file
      url = URI.escape("http://#{request.host}#{@user.current_segment.ad_spot.spot.url}")
      @tropo.say url

    when 'conference'
      # 2. Then Join Conference
      @tropo.say "Joining conference number #{@user.current_segment.id}"
      @tropo.on :event => 'continue', :next => "/conference.json?id=#{@user.id}"

    when 'greeter_conference'
      # 1. Play greeter file
      url = URI.escape("http://#{request.host}#{@user.current_segment.ad_spot.spot.url}")
      @tropo.say url

      # 2. Then Join Conference
      @tropo.say "Joining conference number #{@user.current_segment.id}"
      @tropo.on :event => 'continue', :next => "/conference.json?id=#{@user.id}"
    end

    logger.error "?"*80
    logger.error "create.json: #{@tropo.response}"
    logger.error "?"*80

    respond_to do |format|
      format.json { render :json => @tropo.response }
    end
  end

  # POST /conference.json?id=1
  def conference
    @user = User.find(params[:id])
    @tropo = Tropo::Generator.new
    @tropo.conference :id   => "localdipity-conference-#{@user.current_segment.id}", 
                      :name => "localdipity-conference-#{@user.current_segment.id}"

    logger.error "?"*80
    logger.error "conference.json: #{@tropo.response}"
    logger.error "?"*80

    respond_to do |format|
      format.json { render :json => @tropo.response }
    end
  end
end
