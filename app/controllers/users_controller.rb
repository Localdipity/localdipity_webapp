class UsersController < ApplicationController
  layout 'new_user', :only => :new
  layout 'application', :only => [:show, :index, :greeting_finished]
  layout 'advertagging', :only => :advertagging
  skip_before_filter :verify_authenticity_token, :only => [:create, :update_location, :greeting_finished]

  # GET /users
  # GET /users.xml
  def index
    @users = User.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @users }
    end
  end

  # GET /users/1
  # GET /users/1.xml
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @user }
    end
  end

  # GET /users/new
  # GET /users/new.xml
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  # POST /users.xml
  def create
    @user = User.new(params[:user])

    if params[:lat_lng] and params[:lat_lng].present?
      # Split on a space or a comma
      lat, delim, lng = params[:lat_lng].split(/(\s|,|\t)+/)

      segs = Segment.bounder(lat,lng)
      @user.current_segment = segs
    end

    respond_to do |format|
      if @user.save
        flash[:notice] = 'User was successfully created.'
        format.html { redirect_to(@user) }
        format.xml  { render :xml => @user, :status => :created, :location => @user }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.xml
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        flash[:notice] = 'User was successfully updated.'
        format.html { redirect_to(@user) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.xml
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to(users_url) }
      format.xml  { head :ok }
    end
  end

  # Call and play greeter file associated
  # make it an ajax
  def update_location
    # find by id or by phone number
    user = User.find_by_id(params[:id]) || User.find_by_phone(params[:phone])

    # Split on a space or a comma
    lat, delim, lng = params[:lat_lng].split(/(\s|,|\t)+/)

    segs = Segment.bounder(lat,lng)
    if user.update_attributes(:current_segment => segs)
      render :text => URI.escape("http://#{request.host_with_port}" + segs.ad_spot.spot.url) and return
    end
  end

  def tropo_update_location
    # find by id or by phone number
    user = User.find_by_id(params[:id])

    # Split on a space or a comma
    lat, delim, lng = params[:lat_lng].split(/(\s|,|\t)+/)

    segs = Segment.bounder(lat,lng)
    if user.update_attributes(:current_segment => segs)
      hit_tropo_end_point({
        'id'        => user.id,
        'action'    => 'create',
        'call_type' => params[:call_type] || AppSettings::CALL_TYPE[:greeter]
      })
    end

    respond_to do |format|
      format.js {
        render :update do |page|
          page.alert("You are now in segment: #{segs.description}.")
        end
      }
      # format.html
    end

  end


  # def join_conference
  def greeting_finished
    @user = User.find_by_phone(params[:phone]) || User.find_by_id(params[:id]) 

    # if location is provided, we'll update the location too!
    if @user.present? and params[:lat_lng] and params[:lat_lng].present?
      # Split on a space or a comma
      lat, delim, lng = params[:lat_lng].split(/(\s|,|\t)+/)

      segs = Segment.bounder(lat,lng)
      @user.current_segment = segs
    end

    if @user.present? and @user.current_segment.present?
      hit_tropo_end_point({
        'id'        => @user.id,
        'action'    => 'create',
        'call_type' => AppSettings::CALL_TYPE[:conference]
      })
      flash.now[:notice] = "You are in segment #{@user.current_segment.description}"
    elsif @user.blank?
      flash.now[:notice] = "User #{params[:id]} is not found"
    else
      flash.now[:notice] = "User #{@user.email} is not currently in a segment, did you update your location?"
    end
    render :show
  end

  def demo
    if params[:user_id]
      @user = User.find(params[:user_id]) 
    else
      @user = User.new
    end
  end

  def ask_a_question
    return if params[:phone].blank? or params[:hidden_lat_lng].blank?

    cookies[:phone] = params[:phone]

    user = User.find_or_initialize_by_phone(params[:phone])

    # Split on a space or a comma
    lat, delim, lng = params[:hidden_lat_lng].split(/(\s|,|\t)+/)

    segs = Segment.bounder(lat,lng)
    user.current_segment = segs

    if user.update_attributes(:email => 'from_user_demo_page@example.com')
      hit_tropo_end_point({
        'id'        => user.id,
        'action'    => 'create',
        'call_type' => AppSettings::CALL_TYPE[:conference]
      })
    end
    flash[:notice] = "You are in segment #{user.current_segment.description}"
    redirect_to demo_users_url
  end
end
