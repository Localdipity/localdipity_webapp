package localdipity;

/*
 * Copyright (c) 2010 MH Solutions, LLC.  All rights reserved.
 */

import com.skype.Call;
import com.skype.CallAdapter;
import com.skype.Skype;
import com.skype.SkypeException;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class localdipityClient extends JFrame {
  private String username;
  private String hostname;

  private Socket kkSocket = null;
  private PrintWriter out = null;
  private BufferedReader in = null;

  //******************************************************************************
  // GUI STUFF
  //******************************************************************************
  private JPanel contentPanel;

  // hostname label and text field
  // this will go away eventually
  private JLabel hostLabel;
  private JTextField hostTextField;

  // username label and text field
  // this will go away eventually
  private JLabel unLabel;
  private JTextField unTextField;

  // lat, lng label and text field
  private JLabel latLabel;
  private JTextField latTextField;
  private JLabel lngLabel;
  private JTextField lngTextField;

  // this will go away eventually
  private JButton loginButton;
  private JButton logoutButton;

  private JButton locationButton;
  private JButton psaButton;

  // tracks the responses from the server and displays them for debugging purposes
  // this will go away eventually
  private JTextArea serverResponse;

  private void addElement (Container c, Component e, int x, int y, int h, int w) {
    e.setBounds(x, y, h, w);
    c.add(e);
  }

  // requires that hostname be set
  public void connect() {
    try {
      if (kkSocket == null) {
        kkSocket = new Socket(hostname, 40044);
        out = new PrintWriter(kkSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(kkSocket.getInputStream()));
      }
    } catch (UnknownHostException ee) {
      String msg = "Don't know about host: " + hostname;
      JOptionPane.showMessageDialog(null, msg);
      System.err.println(msg);
      System.exit(1);
    } catch (IOException ee) {
      String msg = "Couldn't get I/O for " + "the connection to: " + hostname;
      JOptionPane.showMessageDialog(null, msg);
      System.err.println(msg);
      System.exit(1);
    }
  }

		public void debugPrint(String msg) {
    if (true)
      System.out.println("threadid: " + Thread.currentThread().getId() + " " + msg);
	}

		public void waitForAllCallsToFinish() throws SkypeException {
 			Call [] active_call_list = Skype.getAllActiveCalls();
			boolean all_finished = false;
			while(!all_finished) {
				all_finished = true;
				debugPrint("waiting for active calls to finish");
				for(int i = 0; i< active_call_list.length; i++) {
					Call call = (Call)active_call_list[i];
					debugPrint("call: " + call.getId() + " " + call.getStatus().toString());
					if(call.getStatus().toString() != "FINISHED" &&
					   call.getStatus().toString() != "MISSED" &&
					   call.getStatus().toString() != "REFUSED") {
						all_finished = false;
					}
				}
			}
	}

  localdipityClient(String un) {
    contentPanel = (JPanel)getContentPane();
    contentPanel.setLayout(null); // a blank form

    // set position for username
    username = un;
    unLabel = new JLabel("Username:");
    addElement(contentPanel, unLabel,10,10,100,20);
    unTextField = new JTextField();
    addElement(contentPanel, unTextField,110,10,100,20);
    unTextField.setText(username);

    // set position for latitude
    latLabel = new JLabel("Latitude:");
    addElement(contentPanel, latLabel,10,110,100,20);
    latTextField = new JTextField();
    addElement(contentPanel, latTextField,110,110,100,20);

    // set position for longitude
    lngLabel = new JLabel("Longitude:");
    addElement(contentPanel, lngLabel,10,130,100,20);
    lngTextField = new JTextField();
    addElement(contentPanel, lngTextField,110,130,100,20);

    // set position for host
    hostLabel = new JLabel("Host: ");
    addElement(contentPanel, hostLabel,10,30,100,20);
    hostTextField = new JTextField();
    hostTextField.setText("app.localdipity.com");
    addElement(contentPanel, hostTextField,110,30,100,20);

    // set position for login button
    loginButton = new JButton("Login");
    addElement(contentPanel, loginButton,10,54,100,20);

    // set position for logout button
    logoutButton = new JButton("Logout");
    addElement(contentPanel, logoutButton,10,74,100,20);

    // set position for location button
    locationButton = new JButton("Send Location");
    addElement(contentPanel, locationButton,10,154,100,20);

    // set position for psa button
    psaButton = new JButton("PSA");
    addElement(contentPanel, psaButton,10,184,100,20);

    // set position for server response log
    serverResponse = new JTextArea("server response here");
    addElement(contentPanel, serverResponse,220,10,400,200);

    loginButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        // check for username and host
        if (0 == unTextField.getText().length()) {
            JOptionPane.showMessageDialog(null, "Please provide a username");
            unTextField.requestFocus();
            return;
        }
        if (0 == hostTextField.getText().length()) {
            JOptionPane.showMessageDialog(null, "Please provide a hostname");
            hostTextField.requestFocus();
            return;
        }
        username = new String(unTextField.getText());
        hostname = new String(hostTextField.getText());

        // connect to host
	connect();

        // once socket is established, send 'LOGIN: username'
	// and send to server
	out.println("LOGIN: " + username);
        out.flush();

        String userInput = null;
        try {
            userInput = in.readLine() + "\n";
            userInput += in.readLine();
            serverResponse.setText(serverResponse.getText() + "\n" + userInput);
            System.out.println("echo: " + userInput);
        }
        catch (Exception ee) {
            System.err.println("some exception when reading server response");
        }
      }
    });

    logoutButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        // check for username and host
        if (0 == unTextField.getText().length()) {
            JOptionPane.showMessageDialog(null, "Please provide a username");
            unTextField.requestFocus();
            return;
        }
        if (0 == hostTextField.getText().length()) {
            JOptionPane.showMessageDialog(null, "Please provide a hostname");
            hostTextField.requestFocus();
            return;
        }
        username = new String(unTextField.getText());
        hostname = new String(hostTextField.getText());

        // connect to host
	connect();

        // once socket is established, send 'LOGIN: username'
	// and send to server
	out.println("LOGOUT: " + username);
        out.flush();

        String userInput = null;
        try {
            userInput = in.readLine() + "\n";
            userInput += in.readLine();
            serverResponse.setText(serverResponse.getText() + "\n" + userInput);
            System.out.println("echo: " + userInput);
        }
        catch (Exception ee) {
            System.err.println("some exception when reading server response");
        }
      }
    });

    locationButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (kkSocket == null) {
            JOptionPane.showMessageDialog(null, "Please login first!");
            unTextField.requestFocus();
            return;
        }
        if (0 == latTextField.getText().length()) {
            JOptionPane.showMessageDialog(null, "Please provide a latitude");
            latTextField.requestFocus();
            return;
        }
        if (0 == lngTextField.getText().length()) {
            JOptionPane.showMessageDialog(null, "Please provide a longitude");
            lngTextField.requestFocus();
            return;
        }

        username = new String(unTextField.getText());
        String lat = new String(latTextField.getText());
        String lng = new String(lngTextField.getText());

				try {
 			    Call [] active_call_list = Skype.getAllActiveCalls();
			    for(int i = 0; i < active_call_list.length; i++) {
				    Call call = (Call)active_call_list[i];

				    // only place a call on hold if its not already on hold
				    String call_status = call.getStatus().toString();
				    if(call_status.equals("INPROGRESS")) {
				      call.finish();
				    }
			    }
					waitForAllCallsToFinish();

				}
				catch (Exception ex) {
					System.err.println("Skype Exception when trying to hang up all existing calls");
				}

				try {
					Thread.currentThread().sleep(500);
				}
				catch (Exception ex) {
					System.err.println("Exception while sleeping before sending location");
				}

        // once socket is established, send 'LOGIN: username'
      	// and send to server
      	out.println("LOCATION: " + username + " " + lat + " " + lng);
        out.flush();

        String userInput = null;
        try {
            userInput = in.readLine() + "\n";
            userInput += in.readLine();
            serverResponse.setText(serverResponse.getText() + "\n" + userInput);
            System.out.println("echo: " + userInput);
        }
        catch (Exception ee) {
            System.err.println("buooga");
        }
      }
    });
    psaButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (kkSocket == null) {
            JOptionPane.showMessageDialog(null, "Please login first!");
            unTextField.requestFocus();
            return;
        }
        if (0 == latTextField.getText().length()) {
            JOptionPane.showMessageDialog(null, "Please provide a latitude");
            latTextField.requestFocus();
            return;
        }
        if (0 == lngTextField.getText().length()) {
            JOptionPane.showMessageDialog(null, "Please provide a longitude");
            lngTextField.requestFocus();
            return;
        }

        username = new String(unTextField.getText());
        String lat = new String(latTextField.getText());
        String lng = new String(lngTextField.getText());

        // once socket is established, send 'LOGIN: username'
	// and send to server
	out.println("PSA_LOCATION: " + username + " " + lat + " " + lng);
        out.flush();

        String userInput = null;
        try {
            userInput = in.readLine() + "\n";
            userInput += in.readLine();
            serverResponse.setText(serverResponse.getText() + "\n" + userInput);
            System.out.println("echo: " + userInput);
        }
        catch (Exception ee) {
            System.err.println("exception writing response from server to GUI");
        }
      }
    });

    setTitle("Localdipity Client");
    setSize(800, 300); // The GUI dimensions
    setLocation(new Point(150, 150)); //The GUI position
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    setVisible(true);
  }


  public static void main(String args[]) throws SkypeException {
    Skype.setDaemon(false); // to prevent exiting from this program
    String un = Skype.getProfile().getId();

    try {
      Skype.addCallListener(new CallAdapter() {
				@Override
				public void callReceived(Call receivedCall) throws SkypeException {
							System.out.println("PARTNER: " + receivedCall.getPartnerId());
							receivedCall.answer();
				}
			});
		} catch (SkypeException ex) {
				Logger.getLogger(localdipityClient.class.getName()).log(Level.SEVERE, null, ex);
		}

    new localdipityClient(un);
  }
}