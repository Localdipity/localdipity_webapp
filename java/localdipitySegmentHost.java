package localdipity;

/*
 * Copyright (c) 2010 MH Solutions, LLC.  All rights reserved.
 */
import com.skype.Call;
import com.skype.Call.Status;
import com.skype.CallAdapter;
import com.skype.CallStatusChangedListener;
import com.skype.Skype;
import com.skype.SkypeException;
import com.skype.connector.Connector;
import java.net.*;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class localdipitySegmentHost {
  private String username;
  private String hostname;
  private Call current_call;
  private boolean current_call_inprogress;
  Call [] active_call_list;
  String skype_name;

  private Socket kkSocket = null;
  private PrintWriter out = null;
  private BufferedReader in = null;

  /****************************************************************************/
  // Constructor
  // Establishes a connection to the server and logs in with the
  // skype username associated with the Skype client.
  /****************************************************************************/
  localdipitySegmentHost(String un) {
    username = un;
    hostname = "app.localdipitiy.com";
    current_call = null;
    current_call_inprogress = false;
    active_call_list = null;
    skype_name = null;

    connect();

    // once socket is established, send 'LOGIN: username'
    // and send to server
    out.println("HOST: LOGIN " + un + " pass");
    out.flush();
  }

  /****************************************************************************/
  // Establish a socket connection to the server Requires that hostname
  // be set
  /****************************************************************************/
  public void connect() {
    try {
      if (kkSocket == null) {
	kkSocket = new Socket(hostname, 40044);
	out = new PrintWriter(kkSocket.getOutputStream(), true);
	in = new BufferedReader(new InputStreamReader(kkSocket.getInputStream()));
      }
    } catch (UnknownHostException ee) {
      String msg = "Don't know about host: " + hostname;
      System.err.println(msg);
      System.exit(1);
    } catch (IOException ee) {
      String msg = "Couldn't get I/O for " + "the connection to: " + hostname;
      System.err.println(msg);
      System.exit(1);
    }
  }
  private void setCurrentCallInProgress(boolean value) {
    current_call_inprogress = value;
  }
  private boolean getCurrentCallInProgress() {
    return current_call_inprogress;
  }

  /****************************************************************************/
  // Whenever a Skype Call experiences a status change, this code will
  // be run.
  /****************************************************************************/
  public void addCallListener() {
    try {
      Skype.addCallListener(new CallAdapter() {
	@Override
	public void callMaked(final Call makedCall) throws SkypeException {
	  makedCall.addCallStatusChangedListener(new CallStatusChangedListener() {
	    public void statusChanged(Status status) throws SkypeException {
	      debugPrint("status: " + status);

	      if((status.toString().equals("REFUSED") || status.toString().equals("INPROGRESS")) && (current_call == makedCall)) {
		setCurrentCallInProgress(true);
	      }

	      if(status.toString().equals("FINISHED")) {
		// TODO: notify switch that user is no longer on call
		//       the switch will decrement the user count
	      }
	    }
	  });
	}
      });
    } catch (SkypeException ex) {
      System.err.println("exception making skype call: " + ex.getMessage());
      ex.printStackTrace();
    }
  }
  public void call() {
    try {
      // insure current call in progress flag is false since we haven't made 
      // the call yet
      setCurrentCallInProgress(false);

      // Tell skype to call the user
      current_call = Skype.call(skype_name);

      // block until call listenter is called for this call
      while(!getCurrentCallInProgress()) {

	debugPrint("yielding until call is established or refused -- call (" + current_call.getId() + ") conference id: " + current_call.getConferenceId());

	Thread.currentThread().yield();
      }

      // reset it now that we are done blocking
      setCurrentCallInProgress(false);

    } catch (SkypeException ex) {
      Logger.getLogger(localdipitySegmentHost.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  public void putAllCallsOnHold() throws SkypeException {
    // Iterate through active calls putting them on hold
    // Also, use the first Call to get the conference id
    active_call_list = Skype.getAllActiveCalls();
    for(int i = 0; i < active_call_list.length; i++) {
      Call call = (Call)active_call_list[i];

      // only place a call on hold if its not already on hold
      String call_status = call.getStatus().toString();
      if(!(call_status.equals("LOCALHOLD") || call_status.equals("HOLD"))) {
	call.hold();
      }
    }
  }
  public void waitForAllCallsToHold() throws SkypeException {
    // Any System.out is debugging in this block
    boolean all_holding = false;
    boolean any_in_progress = false;
    while(!all_holding) {
      debugPrint("waiting for active calls to hold");
      debugPrint("   all_holding" + all_holding);
      debugPrint("   any_in_progress" + any_in_progress);
      any_in_progress = false;
      for(int i = 0; i< active_call_list.length; i++) {
	Call call = (Call)active_call_list[i];
	debugPrint("call: " + call.getId() + " " + call.getStatus().toString());
	if(call.getStatus().toString() != "LOCALHOLD") {
	  any_in_progress = true;
	}
      }
      all_holding = !any_in_progress;
    }
  }
  public String parseInput(String input) {
    // Split input into tokens based on ':'
    String delims ="[:]+";
    String[] tokens = input.split(delims);
    // CALL: skype_name
    //  0       1

    // Debugging output
    for (int i = 0; i < tokens.length; i++) {
      debugPrint("token[" + i + "]: " + tokens[i]);
    }

    // insure we have enough tokens
    if(tokens.length < 2)
      return null;

    // insure this is the call command
    if(!tokens[0].equals("CALL"))
      return null;

    // Listen for CALL command
    debugPrint("received call command");

    return tokens[1].trim();
  }
  public void joinConference() throws Exception {
    String join_id = null;
    active_call_list = Skype.getAllActiveCalls();

    for(int i = 0; i < active_call_list.length; i++) {
      Call call = (Call)active_call_list[i];

      if(call.equals(current_call)) {
	debugPrint("join conference[" + i + "] current_call == call");
	continue;
      }

      if(join_id == null) {
	debugPrint("join conference[" + i + "]. current_call: " + current_call.getId() + " call: " + call.getId());
	String command = "SET CALL " + current_call.getId() + " JOIN_CONFERENCE " + call.getId();
	Connector.getInstance().execute(command);

	join_id = call.getConferenceId();
      }
      else {
	debugPrint("join conference[" + i + "]. current_call: " + current_call.getId() + " call: " + call.getId());
	String command = "SET CALL " + current_call.getId() + " JOIN_CONFERENCE " + join_id;
	Connector.getInstance().execute(command);
      }
    }
  }
  public void debugPrint(String msg) {
    if (true)
      System.out.println("threadid: " + Thread.currentThread().getId() + " " + msg);
  }

  public void run() {
    debugPrint("RUNNING");

    addCallListener();
    String blah = null;

    try {
      LinkedList command_queue = new LinkedList();
      String userInput = null;

      // Read from the socket until it closes or an exception is thrown
      while ((blah = in.readLine()) != null) {

	// Debugging output
	debugPrint("echo: '" + blah +"'");

	// Add commands to a queue in case they fail so we can retry them
	command_queue.add(blah);
	try {
	  userInput = (String) command_queue.removeFirst();
	}
	catch (NoSuchElementException e) {
	  userInput = null;
	  debugPrint("empty command_queue!!");
	}
	while(userInput != null) {
	  skype_name = parseInput(userInput);
	  if (skype_name != null) {
	    try {
	      putAllCallsOnHold();
	      waitForAllCallsToHold();

	      debugPrint("calling " + skype_name);

	      call();
	      joinConference();
	    }
	    catch (Exception se) {
	      // If any exception occurs, we 
	      //   a. log the error
	      //   b. sleep three seconds
	      //   c. try again
	      System.err.println("exception making skype call: " + se.getMessage());
	      se.printStackTrace();
	      System.err.println("sleeping 3 seconds and trying again");
	      command_queue.addFirst(userInput);
	      Thread.sleep(3000);
	    }
	  }
	  userInput = null;
	}
      }
    }
    catch (Exception ee) {
      System.err.println("threadid: " + Thread.currentThread().getId() + " some exception when reading server response");
      System.err.println(ee.getMessage());
      System.err.println(Arrays.toString(ee.getStackTrace()));
    }
    System.err.println("threadid: " + Thread.currentThread().getId() + " exiting function");
    System.exit(0);
  }

  public static void main(String args[]) throws SkypeException {
    Skype.setDaemon(false); // to prevent exiting from this program
    Skype.setDebug(true);
    String un = Skype.getProfile().getId();

    localdipitySegmentHost lg = new localdipitySegmentHost(un);
    lg.run();
  }
}
