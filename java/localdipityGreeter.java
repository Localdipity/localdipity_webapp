package localdipity;

/*
 * Copyright (c) 2010 MH Solutions, LLC.  All rights reserved.
 */

import com.skype.Skype;
import com.skype.SkypeException;
import javax.swing.*;
import java.net.*;
import java.io.*;

public class localdipityGreeter extends JFrame {
  private String username;
  private String hostname;

  private Socket kkSocket = null;
  private PrintWriter out = null;
  private BufferedReader in = null;
  private socketThread st = null;

  // requires that hostname be set
  public void connect() {
    try {
      if (kkSocket == null) {
        kkSocket = new Socket(hostname, 40044);
        out = new PrintWriter(kkSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(kkSocket.getInputStream()));
      }
    } catch (UnknownHostException ee) {
      String msg = "Don't know about host: " + hostname;
      System.err.println(msg);
      System.exit(1);
    } catch (IOException ee) {
      String msg = "Couldn't get I/O for " + "the connection to: " + hostname;
      System.err.println(msg);
      System.exit(1);
    }
  }


  localdipityGreeter(String un) {
    username = un;
    hostname = "app.localdipity.com";

    connect();

    // once socket is established, send 'LOGIN: username'
    // and send to server
    out.println("GREETER: LOGIN " + un + " pass");
    out.println("GREETER: AVAILABLE pass");
    out.flush();

    System.err.println("NEW: starting thread");
    st = new socketThread(in, out);
    st.start();
  }

  public static void main(String args[]) throws SkypeException {
    Skype.setDaemon(false); // to prevent exiting from this program
    String un = Skype.getProfile().getId();

    localdipityGreeter lg = new localdipityGreeter(un);
  }
}