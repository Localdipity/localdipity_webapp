package localdipity;

/*
 * Copyright (c) 2010 MH Solutions, LLC.  All rights reserved.
 */
import com.skype.Call;
import com.skype.Call.Status;
import com.skype.CallAdapter;
import com.skype.CallStatusChangedListener;
import com.skype.Skype;
import com.skype.SkypeException;
import com.skype.connector.Connector;
import java.io.*;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Jonathan R. Wallace
 */
public class socketThread extends Thread {
	private BufferedReader in = null;
		private PrintWriter out = null;

	public socketThread(BufferedReader in, PrintWriter out) {
		super("socketThread");
		this.in = in;
                System.err.println("in starting thread");
		this.out = out;
	}

	@Override
	public void run() {
		try {
			Skype.addCallListener(new CallAdapter() {
				@Override
				public void callMaked(Call makedCall) throws SkypeException {
					makedCall.addCallStatusChangedListener(new CallStatusChangedListener() {
						public void statusChanged(Status status) throws SkypeException {
							System.out.println("status: " + status);
						}
					});
				}
			});
		} catch (SkypeException ex) {
			Logger.getLogger(socketThread.class.getName()).log(Level.SEVERE, null, ex);
		}

    System.out.println("RUNNING");
		String userInput = null;
		try {
			while ((userInput = in.readLine()) != null) {
				if (true) {//userInput == LOGIN SUCCESS
					System.out.println("echo: '" + userInput +"'");
				}
				String delims ="[:]+";
				String[] tokens = userInput.split(delims);
				// CALL: skype_name:file_name:length
				//  0       1          2         3
				for (int i = 0; i < tokens.length; i++) {
					System.out.println("token[" + i + "]: " + tokens[i]);
				}

				if (tokens.length >= 4) {
					// listen for CALL command
					if (tokens[0].equals("CALL") || tokens[0].equals("PSA_CALL")) {
						System.out.println("received call command: " + tokens[0]);
						Call c = null;
						try {
							c = Skype.call(tokens[1].trim());
						}
						catch (Exception se) {
							System.err.println("exception making skype call: " + se.getMessage());
							se.printStackTrace();
						}
						try {
							while ("ROUTING".equals(c.getStatus().toString()) || "RINGING".equals(c.getStatus().toString())) {
								System.out.println(" waiting for call status");
								sleep(500);
							}
						} catch (SkypeException ex) {
							Logger.getLogger(socketThread.class.getName()).log(Level.SEVERE, null, ex);
						}

						String file_loc="C:\\" + tokens[2].trim();
						String command = "ALTER CALL " + c.getId() + " SET_INPUT SOUNDCARD=\"default\" FILE=\"" + file_loc + "\"";
						System.out.println("altering call output to be a wave file: " + command);
						Connector.getInstance().execute(command);

						// sleep the length of the wav file
						int sleep_time = Integer.parseInt(tokens[3].trim());
						System.out.println("sleeping " + sleep_time + " so the wav file can finish");
						sleep(sleep_time);

						// check call status before trying to finish the call
						if ("INPROGRESS".equals(c.getStatus().toString()) ||
									 "RINGING".equals(c.getStatus().toString()) ||
										"ONHOLD".equals(c.getStatus().toString()) ||
									"FINISHED".equals(c.getStatus().toString())) {
							c.finish();
						} // else we mark greeter as available

						// mark greeter as free once call is complete
						String command1 = "GREETER: AVAILABLE pass";
						System.out.println("setting greeter to be available: " + command1);
						out.println(command1);
						out.flush();

						// only a regular CALL hands off to a segment host.. a PSA call does not
						if (tokens[0].equals("CALL")) {
							String command2 = "GREETER: HANDOFF pass";
							out.println(command2);
							out.flush();
						}
					}
				}
			}
		}
		catch (Exception ee) {

				System.err.println("some exception when reading server response");
				System.err.println(ee.getMessage());
				System.err.println(Arrays.toString(ee.getStackTrace()));
		}
		System.err.println("exiting function");
	}
}
