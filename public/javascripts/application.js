// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

jQuery(document).ready(function() {
  // only show phone input field if cookies[:phone] is blank
  if(jQuery('#phone').val() == '') {
    jQuery('#phone').show();
  }
  // only show 'erase phone #' link if cookies[:phone] has a value
  else {
    jQuery('#erase_phone').show();
  }

  jQuery('form#listen input[type=submit]').click(function(e) {
    if(jQuery('#lat_lng').val() == '') {
      alert('Please provide a latitude and longitude separated by a comma');
      jQuery('#lat_lng').select();
      return false;
    }
  });

  jQuery('form#ask_a_question input[type=submit]').click(function () {

    // Ensure we have a phone number
    if(jQuery('#phone').val() == '')  {
      alert('Please provide a phone number');
      jQuery('#phone').select();
      return false;
    }

    // Ensure we have a lat, lng
    if(jQuery('#lat_lng').val() == '') {
      alert('Please provide a latitude and longitude separated by a comma');
      jQuery('#lat_lng').select();
      return false;
    }
    // If we do have one, copy it from the displayed text field to the hidden
    // one for this form
    else {
      jQuery('#hidden_lat_lng').val(jQuery('#lat_lng').val());
    }

    document.cookie = ['phone', '=', encodeURIComponent(jQuery('#phone').val())].join('');

  });

  jQuery('#erase_phone').click(function () {
    alert('Erasing phone number');
    document.cookie = ['phone', '=', encodeURIComponent('')].join('');
    jQuery('#phone').val('');

    // only show phone input field if cookies[:phone] is blank
    if(jQuery('#phone').val() == '') {
      jQuery('#phone').show();
      jQuery('#erase_phone').hide();
    }
    // only show 'erase phone #' link if cookies[:phone] has a value
    else {
      jQuery('#erase_phone').show();
    }
  });
});
